const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens
// JWT is a way of securely passing information from the server to the frontend or to other parts of the server

// Token creation
/*
	Analogy: Pack the gift and provide a lock with  the secret code as the key
*/

module.exports.createAccessToken = (user)=>{
	// the data from the user will be received through forms/req.body
	// when the user logs in, a TOKEN will be created with the user's information

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
}


// Token verification
/*
	Analogy: Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (req,res,next)=>{
	// middlewares which have access ro req,res, and next also can send responses to our client
	// next (a callback to pass control to the next middlware or router)

	// token is retrieved from the request header
	// this can be provided in Postman under Authorization>Bearer Token

	// req.headers.authorization contains sensitive data and especially our token
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth:"Failed. No Token"});
	} else {
		/*
			slice() is a method which can be used on strings and arrays slice(<startPosition>,<endPosition>)

			Bearer asdasg12317sdf (sample)

			"Peter"
			slice(3,string.length)
			"er"
		*/
		console.log("With bearer prefix")
		console.log(token);
		token = token.slice(7,token.length);
		console.log("No bearer prefix")
		console.log(token);

		// Token decryption
		/*
			Analogy: Open the gift and get the content
		*/

		jwt.verify(token, secret, function(err,decodedToken){
			// validate the token using the verify method decrypting the token using the secret code
			// err will contain the error from decoding your token. This will contain the reason why we will reject the token

			if(err){
				return res.send({
					auth:"Failed",
					message: err.message
				})
			} else {
				console.log("Data that will be assigned to  the req.user")
				console.log(decodedToken)
				req.user = decodedToken
				next()
				// middleware function
				// next() will let us proceed to the next middleware or controller
			}
		})
	}
}

// Admin verification
module.exports.verifyAdmin = (req,res,next)=>{
	// verifyAdmin comes after the verify middleware
	if(req.user.isAdmin){
		next()
	} else {
		return res.send ({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}
}