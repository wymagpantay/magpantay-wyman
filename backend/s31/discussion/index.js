console.log("Intro to JSON!");

/*
	JSON
		JSON stands for JS Object Notation
		JSON is also used in other programming languages

		Syntax:

		{
			"property": "valueA",
			"propertyB": "valueB"
		}

		JSON are wrapped in curly braces
		Properties/keys are wrapped in double quotations
*/

	let sample1 = `

		{
			"name":"Jayson Derulo",
			"age": 20,
			"address": {
				"city": "Quezon City",
				"country": "Philippines"
			}

		}

	`;

	console.log(sample1);
	console.log(typeof sample1);

	// Are we able to turn a JSON into a JS Object?
	// YES.

	console.log(JSON.parse(sample1));

	// JSON Array
		// JSON array is an array of JSON

	let sampleArr = `

		[
			{
				"email":"wackywizard@example.com",
				"password":"laughOutLoud123",
				"isAdmin": true
			},
			{
				"email":"dalisay.cardo@gmail.com",
				"password":"alyanna4ever",
				"isAdmin": true
			},
			{
				"email":"lebroan4goat@ex.com",
				"password":"ginisamix",
				"isAdmin": false
			},
			{
				"email":"ilovejson@gmail.com",
				"password":"jsondontloveme",
				"isAdmin": false
			}

		]

	`

	// Can we use Array Methods on a JSON Array?
	// No. Because JSON is a string
	console.log(typeof sampleArr);

	// So what can we do to be able to add more items/objects into our sampleArr?
	// PARSE the JSON array to a JS Array and save it in a variable

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log(typeof parsedSampleArr);

	console.log(parsedSampleArr.pop());
	console.log(parsedSampleArr);

	// if for example, we need to send this data back to our client/frontend, it should be in JSON format 

	// JSON.parse() does not mutate or update the original
		// we can actually turn a JS Object into a JSON again

	//JSON.stringify() - this will stringify JS objects as JSON


	sampleArr = JSON.stringify(parsedSampleArr);
	console.log(sampleArr);
	console.log(typeof sampleArr);

	// Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => Frontend/client

	let jsonArr = `
		[
			"pizza",
			"hamburger",
			"spaghetti",
			"shanghai",
			"hotdog stick on a pineapple",
			"pancit bihon"
		]
	`	
	
	console.log(jsonArr);
	let parsedJsonArr = JSON.parse(jsonArr);
	parsedJsonArr.pop();
	parsedJsonArr.push("sisig");
	jsonArr = JSON.stringify(parsedJsonArr);
	console.log(jsonArr);

	/*// Gather User Details

	let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("Which city do you live in?"),
		country: prompt("Which country does your city address belong to?")
	};

	let otherData = JSON.stringify({

			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address

	})

	console.log(otherData);

	// parse = string to object
	// stringify = objecto to string*/


let users = `[
    {
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    },
    {
      "id": 2,
      "name": "Ervin Howell",
      "username": "Antonette",
      "email": "Shanna@melissa.tv",
      "address": {
        "street": "Victor Plains",
        "suite": "Suite 879",
        "city": "Wisokyburgh",
        "zipcode": "90566-7771",
        "geo": {
          "lat": "-43.9509",
          "lng": "-34.4618"
        }
      },
      "phone": "010-692-6593 x09125",
      "website": "anastasia.net",
      "company": {
        "name": "Deckow-Crist",
        "catchPhrase": "Proactive didactic contingency",
        "bs": "synergize scalable supply-chains"
      }
    },
    {
  "id": 3,
  "name": "Clementine Bauch",
  "username": "Samantha",
  "email": "Nathan@yesenia.net",
  "address": {
    "street": "Douglas Extension",
    "suite": "Suite 847",
    "city": "McKenziehaven",
    "zipcode": "59590-4157",
    "geo": {
      "lat": "-68.6102",
      "lng": "-47.0653"
    }
  },
  "phone": "1-463-123-4447",
  "website": "ramiro.info",
  "company": {
    "name": "Romaguera-Jacobson",
    "catchPhrase": "Face to face bifurcated interface",
    "bs": "e-enable strategic applications"
  }
},
{
    "id": 4,
    "name": "Patricia Lebsack",
    "username": "Karianne",
    "email": "Julianne.OConner@kory.org",
    "address": {
      "street": "Hoeger Mall",
      "suite": "Apt. 692",
      "city": "South Elvis",
      "zipcode": "53919-4257",
      "geo": {
        "lat": "29.4572",
        "lng": "-164.2990"
      }
    },
    "phone": "493-170-9623 x156",
    "website": "kale.biz",
    "company": {
      "name": "Robel-Corkery",
      "catchPhrase": "Multi-tiered zero tolerance productivity",
      "bs": "transition cutting-edge web services"
    }
  },
  {
    "id": 5,
    "name": "Chelsey Dietrich",
    "username": "Kamren",
    "email": "Lucio_Hettinger@annie.ca",
    "address": {
      "street": "Skiles Walks",
      "suite": "Suite 351",
      "city": "Roscoeview",
      "zipcode": "33263",
      "geo": {
        "lat": "-31.8129",
        "lng": "62.5342"
      }
    },
    "phone": "(254)954-1289",
    "website": "demarco.info",
    "company": {
      "name": "Keebler LLC",
      "catchPhrase": "User-centric fault-tolerant solution",
      "bs": "revolutionize end-to-end systems"
    }
  },
    {
      "id": 6,
      "name": "Mrs. Dennis Schulist",
      "username": "Leopoldo_Corkery",
      "email": "Karley_Dach@jasper.info",
      "address": {
        "street": "Norberto Crossing",
        "suite": "Apt. 950",
        "city": "South Christy",
        "zipcode": "23505-1337",
        "geo": {
          "lat": "-71.4197",
          "lng": "71.7478"
        }
      },
      "phone": "1-477-935-8478 x6430",
      "website": "ola.org",
      "company": {
        "name": "Considine-Lockman",
        "catchPhrase": "Synchronised bottom-line interface",
        "bs": "e-enable innovative applications"
      }
    },
    {
      "id": 7,
      "name": "Kurtis Weissnat",
      "username": "Elwyn.Skiles",
      "email": "Telly.Hoeger@billy.biz",
      "address": {
        "street": "Rex Trail",
        "suite": "Suite 280",
        "city": "Howemouth",
        "zipcode": "58804-1099",
        "geo": {
          "lat": "24.8918",
          "lng": "21.8984"
        }
      },
      "phone": "210.067.6132",
      "website": "elvis.io",
      "company": {
        "name": "Johns Group",
        "catchPhrase": "Configurable multimedia task-force",
        "bs": "generate enterprise e-tailers"
      }
    },
    {
      "id": 8,
      "name": "Nicholas Runolfsdottir V",
      "username": "Maxime_Nienow",
      "email": "Sherwood@rosamond.me",
      "address": {
        "street": "Ellsworth Summit",
        "suite": "Suite 729",
        "city": "Aliyaview",
        "zipcode": "45169",
        "geo": {
          "lat": "-14.3990",
          "lng": "-120.7677"
        }
    },
      "phone": "586.493.6943 x140",
      "website": "jacynthe.com",
      "company": {
        "name": "Abernathy Group",
        "catchPhrase": "Implemented secondary concept",
        "bs": "e-enable extensible e-tailers"
      }
    }
,
    {
      "id": 9,
      "name": "Glenna Reichert",
      "username": "Delphine",
      "email": "Chaim_McDermott@dana.io",
      "address": {
        "street": "Dayna Park",
        "suite": "Suite 449",
        "city": "Bartholomebury",
        "zipcode": "76495-3109",
        "geo": {
          "lat": "24.6463",
          "lng": "-168.8889"
        }
      },
      "phone": "(775)976-6794 x41206",
      "website": "conrad.com",
      "company": {
        "name": "Yost and Sons",
        "catchPhrase": "Switchable contextually-based project",
        "bs": "aggregate real-time technologies"
      }
    },
    {
      "id": 10,
      "name": "Clementina DuBuque",
      "username": "Moriah.Stanton",
      "email": "Rey.Padberg@karina.biz",
      "address": {
        "street": "Kattie Turnpike",
        "suite": "Suite 198",
        "city": "Lebsackbury",
        "zipcode": "31428-2261",
        "geo": {
          "lat": "-38.2386",
          "lng": "57.2232"
        }
      },
      "phone": "024-648-3804",
      "website": "ambrose.net",
      "company": {
        "name": "Hoeger LLC",
        "catchPhrase": "Centralized empowering task-force",
        "bs": "target end-to-end models"
      }
    }

]`;
  
let parsedUsers = JSON.parse(users)
console.log(parsedUsers);



  

