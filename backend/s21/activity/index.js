/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

function getUserInfo(){
    return {
        name: "Cardo Dalisay",
        age: 27,
        address: "619 Di-Mahanap Street Di-Malaman City",
        isMarried: false,
        petName: "Tanggol"
    };
};

let userInfo = getUserInfo();
console.log(userInfo);



/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

function getArtistsArray(){
    return ["Coldplay", "The 1975", "Coheed and Cambria", "Blink 182", "Autotelic"];
};

let favoriteArtists = getArtistsArray();
console.log (favoriteArtists);



/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

function getSongsArray(){
    return ["A Sky Full of Stars", "Chocolate", "Welcome Home", "I Miss You", "Languyin"];
};

let favoriteSongs = getSongsArray();
console.log (favoriteSongs);



/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/

function getMoviesArray(){
    return ["Catch Me If You Can", "The Terminal", "Shutter Island", "Inception", "Source Code"];
};

let favoriteMovies = getMoviesArray();
console.log (favoriteMovies);



/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/


function getPrimeNumberArray(){
    return [53, 59, 61, 67, 71];
};

let primeNumberAscending = getPrimeNumberArray();
console.log (primeNumberAscending);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
        getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
        getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
        getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
        getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

    }
} catch(err){


}