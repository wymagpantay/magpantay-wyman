console.log("ES6 Updates!")

// ES6 Updates
	// ES6 is one of the latest versions of wring JS and in fact ONE of the MAJOR updates.

	// let and const
		// are ES6 updates, these are the new standards of creating variables

	// In JS, hoisting allows us to use functions and variables before they are declared
	// BUT this might cause confusion, because of the confusion that var hoisting can create, it is BEST to AVOID using Variables before they are declared

	console.log(varSample);
	var varSample = "Hoist me up!!";

	// if you have used nameVar in other parts of the code, you might be surprised at the output we might get

	var nameVar = "Wyman";

	if(true){
		var nameVar = "Hi!"
	}
	var nameVar = "W";

	console.log(nameVar);

	let name1 = "Wy"

	if(true){
		let name1 = "Hello";
	}

	// let name1 = "Hello World";

	console.log(name1)

	// [** Exponent Operator]
		const firstNum =  8**2;
		console.log(firstNum);


// ${} is a placeholder that is used to embed JS expressions when creating strings using template literals
let name ="Carding";

let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

// multi lines using template literals
const anotherMessage =`

	${name} attended a math competition.

	He won it by solving the problem 8**2 with the solution of ${firstNum}!

`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`);

let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "Web Developer",
	income: 50000,
	expenses: 60000,
};

console.log(`${dev.name} is a ${dev.occupation}.`)
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`)


// [Array Destructuring]
/*
	Allows us to unpack elements in arrays into distinct variables
	Allows us to name array elements with variables instead of using index numbers
	Helps with code readability

	Syntax
		let/const [variableName, variableName, variableName] = array
*/


const fullName = ["Juan", "Dela", "Cruz"];
// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring

const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`);
console.log(fullName);

let fruits = ["Mango", "Grapes", "Guava", "Apple"];
let [fruit1, , fruit3, ] = fruits;
console.log(fruit1);
console.log(fruit3);

let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];

let [payter1,payter2,payter3,payter4, ,payter6] = kupunanNiEugene;
console.log(payter1);
console.log(payter2);
console.log(payter3);
console.log(payter4);
console.log(payter6);

// [Object Destructuring]
/*
	Allows to unpack properties of objects into distinct variables
	Shortens the syntax for accessing properties from objects
	Syntax:
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person ={
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// pre-object destructuring:
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

// Object Destructuring
const { givenName, maidenName, familyName } = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);


// [Arrow Functions]
/*
	Compact alternative syntax to traditional functions
	Useful for code snippets where creating functions will not be reused in a ny other portion of the code
	Adhere to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

function displayMsg(){
	console.log("Hi");
}

displayMsg();

let displayHello = () =>{
	console.log("Hello World!")
}

/*displayHello();

function printFullName(firstName,middleInitial,lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John","D","Smith");*/

let printFullName = (firstName,middleInitial,lastName) =>{
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("Jograts","Burachi","Kamote")

const students = ["John","Jane","Natalia","Jobert","Joe"];

// arrow function with loops


// pre-arrow
students.forEach(function(student){
	console.log(`${student} is a student!`);
})

// arrow
students.forEach((student)=>{
	console.log(`${student} is a student. (from arrow)`)
})


// [Implicit Return Statement]
/*
	There are instances when you can omot the "return" statement
	This works because even without the "return" statement JS implicitly adds it for the result of the function
*/

/*function add (x,y){
	return x + y;
}

let total = add(1,2);
console.log(total);*/

const add = (x,y) => x + y;

let total = add (1,2);
console.log(total);

// [Default Function Argument Value]

	const greet = (name = "User") => {
		return `Good morning, ${name}!`
	}

	console.log(greet());
	console.log(greet("John"));

// [Class-Based Object Blueprints]
/*
		Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class
	- "this" keyword referes to the properties of an object created/initialized from the class
*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	let myCar = new Car();
	console.log(myCar);

	myCar.brand = "Ford"
	myCar.name = "Ranger Raptor";
	myCar.year = 2021;

	console.log(myCar);

	const myNewCar = new Car("Toyota","Vios",2021);
	console.log(myNewCar);

// Traditional functions vs Arrow Function as methods

let character1 = {

	name:"Cloud Strife",
	occupation:"Soldier",
	greet: ()=>{

		// In a traditional function:
			// this keyword refers to the current object where the method is
		console.log(this);
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob: function(){
		console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`);
	}
}

character1.greet();
character1.introduceJob();


class Character  {
	constructor(name, role, strength, weakness){
	this.name = name;
	this.role = role;
	this.strength = strength;
	this.weakness = weakness;
	}
}

dwarvenSniper = new Character("Kardel Sharpeye", "Dwarven Sniper", "Range Attacks", "Crowd Control");

console.log(dwarvenSniper)



