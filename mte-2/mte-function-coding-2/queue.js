let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection
}  

function dequeue() {
    delete collection[0];

    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }

    collection[collection.length - 1] = undefined;
    collection.length -= 1;
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    let totalSize = collection.length; 
    return totalSize;
}

function isEmpty() {
    return collection.length === 0
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};