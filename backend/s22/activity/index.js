
	
	// 1. Create a function called addNum which will be able to add two numbers.
	// 	- Numbers must be provided as arguments.
	// 	- Return the result of the addition.
		
		function addNum(num1, num2){
			return num1 + num2;
		};

		let sumOfTwoDigits = addNum(5,6);
		console.log("Displayed sum of 5 and 6");
		console.log(sumOfTwoDigits)

	   
	// Create a function called subNum which will be able to subtract two numbers.
	//  - Numbers must be provided as arguments.
	//  - Return the result of subtraction.

		function subtractNum(num1, num2){
			return num1 - num2;
		};

		let differenceOfTwoDigits = subtractNum(6,5);
		console.log("Displayed difference of 6 and 5");
		console.log(differenceOfTwoDigits)

	//     Create a new variable called sum.
	//      - This sum variable should be able to receive and store the result of addNum function.

		function addNum(num1, num2){
			return num1 + num2;
			console.log("This will not be printed!");
	};
		let sum = addNum(1,2);

	//     Create a new variable called difference.
	//      - This difference variable should be able to receive and store the result of subNum function.

		function subtractNum(num1, num2){
			return num1 - num2;
			console.log("This will not be printed!");
	};
		let difference = subtractNum(2,1);

	//     Log the value of sum variable in the console.
	//     Log the value of difference variable in the console.

	// 2. Create a function called multiplyNum which will be able to multiply two numbers.
	// 	- Numbers must be provided as arguments.
	// 	- Return the result of the multiplication.

		function multiplyNum(num1, num2){
			return num1 * num2;
		};

		let productOfTwoDigits = multiplyNum(9,9);
		console.log("Displayed product of 9 and 9");
		console.log(productOfTwoDigits)

	// 	Create a function divideNum which will be able to divide two numbers.
	// 	- Numbers must be provided as arguments.
	// 	- Return the result of the division.

		function divideNum(num1, num2){
			return num1 / num2;
		};

		let quotientOfTwoDigits = divideNum(90,9);
		console.log("Displayed quotient of 90 and 9");
		console.log(quotientOfTwoDigits)

	// 	Create a new variable called product.
	// 	 - This product variable should be able to receive and store the result of multiplyNum function.

		function multiplyNum(num1, num2){
			return num1 * num2;
			console.log("This will not be printed!");
	};
		let product = multiplyNum(9,9);

	// 	Create a new variable called quotient.
	// 	 - This quotient variable should be able to receive and store the result of divideNum function.

		function divideNum(num1, num2){
			return num1 / num2;
			console.log("This will not be printed!");
	};
		let quotient = divideNum(90,9);

	// 	Log the value of product variable in the console.
	// 	Log the value of quotient variable in the console.


	// 3. Create a function called getCircleArea which will be able to get total area of a circle from a provided radius.
	// 	- a number should be provided as an argument.
	// 	- look up the formula for calculating the area of a circle with a provided/given radius.
	// 	- look up the use of the exponent operator.
	// 	- return the result of the area calculation.

	const pi = 3.1416

		function getCircleArea(radius){
		return pi * radius**2
	};

	let areaCircle = getCircleArea(15);

	console.log("The result of getting the area of a circle with a radius of 15: ")
	console.log(areaCircle);

	// 	Create a new variable called circleArea.
	// 	- This variable should be able to receive and store the result of the circle area calculation.
	// 	- Log the value of the circleArea variable in the console.

	function circleAreaCalculation(pi, radius){
			num2 = radius;
			return pi * radius**2;
			console.log("This will not be printed!");
	};
		let circleArea = circleAreaCalculation(3.1416,15);

	// 4. Create a function called getAverage which will be able to get total average of four numbers.
	// 	- 4 numbers should be provided as an argument.
	// 	- look up the formula for calculating the average of numbers.
	// 	- return the result of the average calculation.

		function getAverage(num1, num2, num3, num4){
			return (num1 + num2 + num3 + num4) / 4 ;
	};
		let averageOf4Digits = getAverage(20, 40, 60, 80, 5);
		console.log("The average of 20, 40, 60, and 80: ");
		console.log(averageOf4Digits);
		
	// 	Create a new variable called averageVar.
	// 	- This variable should be able to receive and store the result of the average calculation
	// 	- Log the value of the averageVar variable in the console.

		function averageCalculation(num1, num2, num3){
			return (num1 + num2 + num3) / 3
			console.log("This will not be printed!");
	};
		let averageVar = averageCalculation(1,2,3);


	// 5. Create a function called checkIfPassed which will be able to check if you passed by checking the percentage of your score against the passing percentage.
	// 	- this function should take 2 numbers as an argument, your score and the total score.
	// 	- First, get the percentage of your score against the total. You can look up the formula to get percentage.
	// 	- Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
	// 	- return the value of the variable isPassed.
	// 	- This function should return a boolean.


		function checkIfPassed(num1,num2){
			num2 = 50;
			return (num1 / num2) * 100;
			// return (score/total)*100 > 75;
			console.log(checkIfPassed);
			let scorePercentage = checkIfPassed(38,50);
			console.log(scorePercentage);
			console.log("This will not be printed!");
		}
		scorePercentage = checkIfPassed(38,50);
		console.log("Is 38/50 a passing score?")		
		let isPassed = scorePercentage > 75
		console.log(isPassed);

		

	

	// 	Create a global variable called outside of the function called isPassingScore.
	// 		-This variable should be able to receive and store the boolean result of the checker function.
	// 		-Log the value of the isPassingScore variable in the console.


function globalChecker(num1,num2){
			num2 = 50;
			return (num1 / num2) * 100;
			console.log(globalChecker);
			let scorePercentage = globalChecker(18,50);
			console.log(scorePercentage);
			console.log("This will not be printed!");
		}
scorePercentage = globalChecker(22,50)	
let isPassingScore = scorePercentage > 75
// console.log(isPassingScore);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		addNum: typeof addNum !== 'undefined' ? addNum : null,
		subNum: typeof subNum !== 'undefined' ? subNum : null,
		multiplyNum: typeof multiplyNum !== 'undefined' ? multiplyNum : null,
		divideNum: typeof divideNum !== 'undefined' ? divideNum : null,
		getCircleArea: typeof getCircleArea !== 'undefined' ? getCircleArea : null,
		getAverage: typeof getAverage !== 'undefined' ? getAverage : null,
		checkIfPassed: typeof checkIfPassed !== 'undefined' ? checkIfPassed : null,

	}
} catch(err){

}