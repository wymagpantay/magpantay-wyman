// Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course")
const auth = require("../auth")
const {verify, verifyAdmin} = auth;

// Routing Component
const router = express.Router();

// create a course
router.post("/", verify, verifyAdmin, courseController.addCourse)

// route for retrieving all courses
router.get("/all",courseController.getAllCourses);

// create a route for getting all active courses
// use default endpoint
// getAllActiveCourses

router.get("/",courseController.getAllActiveCourses);

// get specific course
router.get("/:courseId",courseController.getCourse);

// edit a specific course
router.put("/:courseId",verify, verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive",verify, verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate",verify, verifyAdmin, courseController.activateCourse);



module.exports = router;