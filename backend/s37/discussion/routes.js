// Servers can actually respond differently with different requests

// We start our request with our URL
// A client can start a different request with a different URL

/*http://localhost:4000/

http://localhost:4000/profile*/

// http://localhost:4000/ is not the same as http://localhost:4000/profile

/*
	/ = url endpoint (default)
	/profile = url endpoint
*/

const http = require('http');
// creates a variable port to store the port number (reusability)

const port = 4000;

const app = http.createServer((request,response)=>{

	/*
		Information about the URL endpoint of the req is in the request object
		Different request, require different response
		The process or way to respon differently to a request is called a ROUTE
	*/

	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('Hello, B297!')
	}
	else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('This is the homepage')
	}

	else {
		response.writeHead(404,{'Content-type':'text/plain'})
		response.end('Page note available')
	}

})

app.listen(port);
console.log(`Server is now accessible at localhost: ${port}`)
