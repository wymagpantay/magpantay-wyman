console.log("Hello, B297!")

// Functions
	// Parameters and Arguments

/*	function printName(){

		let nickname = prompt("Enter your nickname: ");
		console.log("Hi, " + nickname);

	}

	printName();*/


function printName(name){

		console.log("Hi, " + name);

	}

	printName("Wyman");

	let sampleVariable = "Jobert";

	printName(sampleVariable);


	function checkDivisivilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is "  + num + " divisible by 8?");
		console.log(isDivisibleBy8);


	}

	checkDivisivilityBy8(64);
	checkDivisivilityBy8(28);
	checkDivisivilityBy8(9678);


	function checkDivisivilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is "  + num + " divisible by 4?");
		console.log(isDivisibleBy4);

}

	checkDivisivilityBy4(64);
	checkDivisivilityBy4(28);
	checkDivisivilityBy4(9678);

	// Functions as arguments
	// Functions parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};


	// Adding and removing the parentheses "()" impacts the output of JS heavily
	//  when a function is used with parentheses "()". it denotes invoking/calling a 
	// function

	// A function used without parenthesis "()" is normally associated with using the function as
	// an argument to another function.

	invokeFunction(argumentFunction);

	// Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	};

	createFullName("Juan", "Dela", "Cruz");
	createFullName("Jograts", "Dela", "Monggoloid");
	createFullName("Jograts", "Dela");
	createFullName("Jograts", "Dela", "Monggoloid", "III");

	// Using variables as arguments

	let firstName = "Jograts"
	let middleName = "Ungasis"
	let lastName = "Tolonggis"

	createFullName(firstName, middleName, lastName);

	// 

	let friend1 = "Me"
	let friend2 = "Myself"
	let friend3 = "I"

	function printFriends(friend1, friend2, friend3){
		console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3);

	}

	printFriends("Me", "Myself", "I");


	// Return Statement

	function returnFullName(firstName, middleName, lastName){
		return firstName + " " + middleName + " " + lastName;
		console.log("This will not be printed!");
	}

	let completeName1 = returnFullName("Monkey", "D", "Luffy");
	let completeName2 = returnFullName("Engot", "Ka", "Kase");

	console.log(completeName1 + " is my bestfriend!");
	console.log(completeName2 + " is my friend!");


	function getSquareArea(side){
		return side**2
	};

	let areaSquare = getSquareArea(4);

	console.log("The result of getting the area of a square with length of 4:")
	console.log(areaSquare);


	function computeSum(num1, num2, num3){
		return num1+num2+num3;
	};

	let sumOfThreeDigits = computeSum(1,2,3);
	console.log("The sum of three numbers are: ");
	console.log(sumOfThreeDigits);

	function compareToOneHundred(num){
		return num === 100;
	};

	let booleanValue = compareToOneHundred(99);
	console.log("Is this one hundred?");
	console.log(booleanValue);