let gradingSystem = () => {
  let average = parseInt(prompt("What is your Average?"));
  if (average <= 80) {
    return "Your grade is: " + average + ". You're only a Novice :(";
  } else if (average >= 81 && average <= 90) {
    return "Your grade is: " + average + ". You're an Apprentice :|";
  } else if (average >= 91 && average <= 100) {
    return "Your grade is: " + average + ". You're already a Master :)";
  }
}

console.log(gradingSystem());